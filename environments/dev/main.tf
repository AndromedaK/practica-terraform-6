module "azure_key_vault"{
  source          = "../modules/key_vault"
  global_location = var.global_location
  global_rs_name  = var.global_rs_name
  environment     = var.environment
  secrets         = var.secret_key_value 
}