
variable global_location {
  default     = "eastus2"
  description = "Azure region"
}
variable global_rs_name {
  default     = "demo-devops"
  description = "Nombre del grupo de recursos"
}

variable secret_key_value{
     default{          
       USERNAME-BBDD                    = "@undefined"
       PASSWORD-BBDD                    = "@undefined"
     }
}

variable environment{
  default = ""
}