variable secret_key_value{
    type ="map"
    description = "List of secrets for the Key Vault."
    default     = []
}

variable global_location{
    type = string 
}

variable global_rs_name{
    type = string 
}

variable global_environment {
    type = string 
}