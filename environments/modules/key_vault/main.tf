data "azurerm_client_config" "current" {}

locals{
    global_location    = var.global_location
    global_rs_name     = var.global_rs_name 
    global_environment = var.global_environment

}

resource "azurerm_key_vault" "azkclm" {
  name                        = var.keyvault_name
  location                    = local.global_location
  resource_group_name         = local.global_rs_name
  enabled_for_disk_encryption = true
  tenant_id                   = data.azurerm_client_config.current.tenant_id
  sku_name                    = "standard"

  access_policy {
    tenant_id = data.azurerm_client_config.current.tenant_id
    object_id = var.kv_object_id

    key_permissions = [
      "get", "create", "list", "delete",
    ]
    secret_permissions = [
      "get", "set", "list", "delete",
    ]
    storage_permissions = [
      "get", "set", "list", "delete",
    ]
  }

   access_policy {
    tenant_id = data.azurerm_client_config.current.tenant_id
    object_id = var.sp_object_id

    key_permissions = [
      "get", "list",
    ]
    secret_permissions = [
      "get", "list",
    ]
    storage_permissions = [
      "get", "list",
    ]
  }

  tags = {
    environment = local.global_environment
  }
}

resource "azurerm_key_vault_secret" "secretsclm" {
  for_each     = var.secret_key_value
  name         = each.key
  value        = each.value
  key_vault_id = azurerm_key_vault.azkclm.id

  tags = {
    environment = local.global_environment
  }
}

